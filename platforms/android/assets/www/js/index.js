document.addEventListener("deviceready", onDeviceReady, false);

var user_session = "pratiku@webplanex.com";

function gotoPage(goto){
    $.mobile.pageContainer.pagecontainer("change", "#"+goto,{transition: "none"})
    //$.mobile.changePage("#"+goto);
}
function onDeviceReady() {
    document.addEventListener("backbutton", handleBackButton, false);
    // var checkUser = window.localStorage.getItem("user_email");
    // if(checkUser)
    // {
    //     gotoPage("home");    
    // }else{
    //     gotoPage("login");
    // }
    gotoPage("home");  
}
function handleBackButton() {
    gotoPage("passwordscreen");
}

document.addEventListener("deviceready", function () {
    // $("#password_form").validate({
    //     rules: {
    //         textpass: {
    //             required: true
    //         }
    //     },
    //     messages: {
    //         textpass: {
    //             required: "Password required !!."
    //         }
    //     },
    //     errorPlacement: function (error, element) {
    //         error.appendTo(element.parent().prev());
    //     },
    //     submitHandler: function (form) {
    //         alert("ASDFASDF");
    //         // $(':mobile-pagecontainer').pagecontainer('change', '#success', {
    //         //     reload: false
    //         // });
    //         // return false;
    //     }
    // });

    //Enter password to unlock the screen
    $("#submit_pass").on("click",function(){
        var default_password = "password123";
        var password = $("#textpass").val();
        if(password == default_password)
        {
            $("#textpass").val("");
            window.plugins.insomnia.allowSleepAgain();
            $("#checkbox-1a").attr("checked",false).checkboxradio("refresh");
            $("#checkbox-2a").attr("checked",false).checkboxradio("refresh");
            gotoPage("home");
        }
        else{
            //alert("Invalid password. !!");
            window.plugins.toast.show('Invalid password. !!', 'short', 'center', function(a){console.log('toast success: ' + a)}, function(b){alert('toast error: ' + b)})
        }
    });

    $("#validate_user").on("click",function(){
        var login_email = $("#login_email").val();
        if(login_email == user_session)
        {
            $("#login_email").val("");
            window.localStorage.setItem("user_email","pratiku@webplanex.com");

            var push = PushNotification.init({ "android": {"senderID": "186211638697","forceShow":"true"}, "ios": {"alert": "true", "badge": "true", "sound": "true"}, "windows": {} } ); 
            push.on('registration', function(data) {
                console.log(data.registrationId);
                window.localStorage.setItem("gcm_id",data.registrationId);
            });
            push.on('notification', function(data) {
                console.log(data.message);
                alert(data.title+" Message: " +data.message);
                // data.title,
                // data.count,
                // data.sound,
                // data.image,
                // data.additionalData
            });
            push.on('error', function(e) {
                //console.log(e.message);
                alert(e.message);
            });

            gotoPage("home");

        }
        else{
            //alert("Invalid password. !!");
            window.plugins.toast.show('Invalid Login details. !!', 'short', 'center', function(a){console.log('toast success: ' + a)}, function(b){alert('toast error: ' + b)})
        }
    });

    $("#logout_user").on("click",function(){
            window.localStorage.setItem("user_email","");
            gotoPage("login");
    });

    $("#checkbox-1a").on("click",function(){
        if($(this).is(":checked"))
        {
            window.plugins.insomnia.keepAwake();
            console.log("awake");
        }
        else{
            window.plugins.insomnia.allowSleepAgain();
            console.log("sleep");
        }
    });
});